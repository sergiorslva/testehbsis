﻿using HBSIS.Domain.Interface;
using HBSIS.Domain.Model;
using HBSIS.Infra.Data.Context;
using System.Linq;

namespace HBSIS.Infra.Data.Repository
{
    public class LivroRepository : Repository<Livro>, ILivroRepository
    {
        public LivroRepository(HBSISContext context) : base(context)
        {
        }

        public bool IsExisteTituloCadastroComMesmoTitulo(string titulo)
        {
            return this.Db.Livro.Where(x => x.Titulo == titulo).Count() > 1;
        }
    }
}
