﻿using HBSIS.Domain.Interface;
using HBSIS.Domain.Model;
using HBSIS.Infra.Data.Context;
using System.Collections.Generic;
using System.Linq;

namespace HBSIS.Infra.Data.Repository
{
    public class ExemplarRepository : Repository<Exemplar>, IExemplarRepository
    {
        public ExemplarRepository(HBSISContext context) : base(context)
        {
        }

        public IEnumerable<Exemplar> BuscarPorIdLivro(int idLivro)
        {
            return this.Db.Exemplar.Where(x => x.LivroId == idLivro && x.Ativo);
        }

        public bool IsExisteExemplarCadastroParaOutroTitulo(string numero)
        {
            return (from livro in this.Db.Livro
                          join exemplar in this.Db.Exemplar on livro.Id equals exemplar.LivroId
                          where exemplar.Numero == numero
                          select livro).Count() > 0;
        }
    }
}
