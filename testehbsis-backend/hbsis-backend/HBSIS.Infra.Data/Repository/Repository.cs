﻿using HBSIS.Domain.Interface;
using HBSIS.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HBSIS.Infra.Data.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected HBSISContext Db;
        protected DbSet<TEntity> DbSet;

        public Repository(HBSISContext context)
        {
            Db = context;
            DbSet = Db.Set<TEntity>();
        }

        public TEntity Alterar(TEntity obj)
        {
            DbSet.Update(obj);
            Db.SaveChanges();

            return obj;
        }

        public TEntity BuscarPorId(int id)
        {
            return DbSet.Find(id);
        }

        public IEnumerable<TEntity> BuscarTodos()
        {
            return DbSet.AsEnumerable();
        }        

        public TEntity Excluir(TEntity obj)
        {            
            DbSet.Update(obj);
            Db.SaveChanges();

            return obj;
        }

        public TEntity Incluir(TEntity obj)
        {
            DbSet.Add(obj);
            Db.SaveChanges();

            return obj;
        }
                
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
