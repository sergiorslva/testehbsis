﻿using HBSIS.Domain.Model;
using HBSIS.Infra.Data.Mappings;
using Microsoft.EntityFrameworkCore;

namespace HBSIS.Infra.Data.Context
{
    public class HBSISContext: DbContext
    {
        public HBSISContext(DbContextOptions<HBSISContext> options) : base(options)
        {        
        }

        public DbSet<Livro> Livro { get; set; }
        public DbSet<Exemplar> Exemplar { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new LivroMap());
            modelBuilder.ApplyConfiguration(new ExemplarMap());
        }
    }
}
