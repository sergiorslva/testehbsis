﻿using HBSIS.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HBSIS.Infra.Data.Mappings
{
    public class ExemplarMap : IEntityTypeConfiguration<Exemplar>
    {
        public void Configure(EntityTypeBuilder<Exemplar> builder)
        {
            builder.HasKey(c => c.Id);

            builder.Property(e => e.LivroId)
              .HasColumnName("LivroId");

            builder.Property(e => e.Numero)
              .HasColumnName("Numero");
            
            builder.Property(e => e.Ativo)
                .HasColumnName("Ativo");

            builder.HasOne(x => x.Livro).WithMany(x => x.Exemplares).HasForeignKey(f => f.LivroId);

            builder.ToTable("Exemplar");
        }
    }
}
