﻿using HBSIS.Domain.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HBSIS.Infra.Data.Mappings
{
    public class LivroMap : IEntityTypeConfiguration<Livro>
    {     

        public void Configure(EntityTypeBuilder<Livro> builder)
        {
            builder.HasKey(c => c.Id);

            builder.Property(e => e.Titulo)
              .HasColumnName("Titulo");

            builder.Property(e => e.Ano)
              .HasColumnName("Ano");

            builder.Property(e => e.Escritor)
              .HasColumnName("Escritor");

            builder.Property(e => e.Ativo)
                .HasColumnName("Ativo");

            builder.ToTable("Livro");
        }
    }
}
