﻿using AutoMapper;
using HBSIS.Application.Interface;
using HBSIS.Application.Service;
using HBSIS.Domain.Interface;
using HBSIS.Domain.Service;
using HBSIS.Infra.Data.Context;
using HBSIS.Infra.Data.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace HBSIS.Infra.CrossCutting.IoC
{
    public class NativeInjectorBootStrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            // ASPNET                        
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            //Repository            
            services.AddScoped<DbContext, HBSISContext>();
            services.AddScoped<ILivroRepository, LivroRepository>();
            services.AddScoped<IExemplarRepository, ExemplarRepository>();

            //Services  
            services.AddScoped<ILivroService, LivroService>();
            services.AddScoped<IExemplarService, ExemplarService>();

            //Application     
            services.AddScoped<ILivroApp, LivroApp>();
            services.AddScoped<IExemplarApp, ExemplarApp>();
        }
    }
}
