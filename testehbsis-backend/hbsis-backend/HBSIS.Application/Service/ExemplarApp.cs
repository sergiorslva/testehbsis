﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using HBSIS.Application.Interface;
using HBSIS.Application.ViewModel;
using HBSIS.Domain.Interface;
using HBSIS.Domain.Model;

namespace HBSIS.Application.Service
{
    public class ExemplarApp : IExemplarApp
    {
        private readonly IExemplarService exemplarService;
        private readonly IMapper mapper;

        public ExemplarApp(IExemplarService exemplarService, IMapper mapper)
        {
            this.exemplarService = exemplarService;
            this.mapper = mapper;
        }

        public ExemplarViewModel Alterar(ExemplarViewModel exemplarViewModel)
        {
            var exemplar = this.mapper.Map<Exemplar>(exemplarViewModel);
            return this.mapper.Map<ExemplarViewModel>(this.exemplarService.Alterar(exemplar));
        }

        public ExemplarViewModel BuscarPorId(int id)
        {
            var exemplar = this.exemplarService.BuscarPorId(id);
            return this.mapper.Map<ExemplarViewModel>(exemplar);
        }

        public IEnumerable<ExemplarViewModel> BuscarTodosPorLivro(int idLivro)
        {
            var exemplares = this.exemplarService.BuscarPorIdLivro(idLivro);
            return this.mapper.Map<List<ExemplarViewModel>>(exemplares);
        }

        public ExemplarViewModel Excluir(int id)
        {
            return this.mapper.Map<ExemplarViewModel>(this.exemplarService.Excluir(id));
        }

        public ExemplarViewModel Incluir(ExemplarViewModel exemplarViewModel)
        {
            var livro = this.mapper.Map<Exemplar>(exemplarViewModel);
            return this.mapper.Map<ExemplarViewModel>(this.exemplarService.Incluir(livro));
        }
    }
}
