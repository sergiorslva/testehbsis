﻿using System.Collections.Generic;
using AutoMapper;
using HBSIS.Application.Interface;
using HBSIS.Application.ViewModel;
using HBSIS.Domain.Interface;
using HBSIS.Domain.Model;

namespace HBSIS.Application.Service
{
    public class LivroApp : ILivroApp
    {
        private readonly ILivroService livroService;
        private readonly IMapper mapper;

        public LivroApp(ILivroService livroService, IMapper mapper)
        {
            this.livroService = livroService;
            this.mapper = mapper;
        }

        public LivroViewModel Alterar(LivroViewModel livroViewModel)
        {
            var livro = this.mapper.Map<Livro>(livroViewModel);
            return this.mapper.Map<LivroViewModel>(this.livroService.Alterar(livro));            
        }

        public LivroViewModel BuscarPorId(int id)
        {
            var livro = this.livroService.BuscarPorId(id);
            return this.mapper.Map<LivroViewModel>(livro);
        }

        public IEnumerable<LivroViewModel> BuscarTodos()
        {
            var livros = this.livroService.BuscarTodos();
            return this.mapper.Map<List<LivroViewModel>>(livros);
        }

        public LivroViewModel Excluir(int id)
        {            
            return this.mapper.Map<LivroViewModel>(this.livroService.Excluir(id));
        }

        public LivroViewModel Incluir(LivroViewModel livroViewModel)
        {
            var livro = this.mapper.Map<Livro>(livroViewModel);
            return this.mapper.Map<LivroViewModel>(this.livroService.Incluir(livro));
        }
    }
}
