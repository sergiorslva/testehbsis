﻿namespace HBSIS.Application.ViewModel
{
    public class ExemplarViewModel
    {
        public int Id { get; set; }
        public int LivroId { get; set; }
        public string Numero { get; set; }
        public bool Ativo { get; set; }
    }
}
