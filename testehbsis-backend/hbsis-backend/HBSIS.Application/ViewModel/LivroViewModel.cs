﻿namespace HBSIS.Application.ViewModel
{
    public class LivroViewModel
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public int Ano { get; set; }
        public string Escritor { get; set; }
        public bool Ativo { get; set; }
    }
}
