﻿using HBSIS.Application.ViewModel;
using System.Collections.Generic;

namespace HBSIS.Application.Interface
{
    public interface IExemplarApp
    {
        ExemplarViewModel Incluir(ExemplarViewModel exemplarViewModel);
        ExemplarViewModel Alterar(ExemplarViewModel exemplarViewModel);
        ExemplarViewModel Excluir(int id);
        ExemplarViewModel BuscarPorId(int id);
        IEnumerable<ExemplarViewModel> BuscarTodosPorLivro(int idLivro);
    }
}
