﻿using HBSIS.Application.ViewModel;
using System.Collections.Generic;

namespace HBSIS.Application.Interface
{
    public interface ILivroApp
    {
        LivroViewModel Incluir(LivroViewModel livroViewModel);
        LivroViewModel Alterar(LivroViewModel livroViewModel);
        LivroViewModel Excluir(int id);
        LivroViewModel BuscarPorId(int id);
        IEnumerable<LivroViewModel> BuscarTodos();
    }
}
