﻿using AutoMapper;
using HBSIS.Application.ViewModel;
using HBSIS.Domain.Model;

namespace HBSIS.Application.AutoMapper
{
    public class DomainToViewModelProfile : Profile
    {
        public DomainToViewModelProfile()
        {
            CreateMap<Livro, LivroViewModel>();
            CreateMap<Exemplar, ExemplarViewModel>();
        }
    }
}
