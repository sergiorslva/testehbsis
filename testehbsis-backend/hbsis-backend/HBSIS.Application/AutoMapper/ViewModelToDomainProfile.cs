﻿using AutoMapper;
using HBSIS.Application.ViewModel;
using HBSIS.Domain.Model;

namespace HBSIS.Application.AutoMapper
{
    public class ViewModelToDomainProfile : Profile
    {
        public ViewModelToDomainProfile()
        {
            CreateMap<LivroViewModel, Livro>();
            CreateMap<ExemplarViewModel, Exemplar>();
        }
    }
}
