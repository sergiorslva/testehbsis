﻿using System;
using System.Collections.Generic;
using System.Linq;
using HBSIS.Domain.Interface;
using HBSIS.Domain.Model;

namespace HBSIS.Domain.Service
{
    public class LivroService : ILivroService
    {
        private readonly ILivroRepository livroRepository;

        public LivroService(ILivroRepository livroRepository)
        {
            this.livroRepository = livroRepository;
        }

        public Livro Alterar(Livro livro)
        {
            return this.livroRepository.Alterar(livro);
        }

        public Livro BuscarPorId(int id)
        {
            return this.livroRepository.BuscarPorId(id);
        }

        public IEnumerable<Livro> BuscarTodos()
        {
            return this.livroRepository.BuscarTodos().Where(x => x.Ativo == true);
        }

        public Livro Excluir(int id)
        {
            var livro = this.livroRepository.BuscarPorId(id);
            livro.Ativo = false;
            return this.livroRepository.Excluir(livro);
        }

        public Livro Incluir(Livro livro)
        {
            var livroJaCadastrado = this.livroRepository.IsExisteTituloCadastroComMesmoTitulo(livro.Titulo); 
            if (livroJaCadastrado)
            {
                throw new Exception("Ja existe um livro cadastrado com esse título"); 
            }

            return this.livroRepository.Incluir(livro);
        }
    }
}
