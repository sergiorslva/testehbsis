﻿using System;
using System.Collections.Generic;
using HBSIS.Domain.Interface;
using HBSIS.Domain.Model;

namespace HBSIS.Domain.Service
{
    public class ExemplarService : IExemplarService
    {
        private readonly IExemplarRepository exemplarRepository;

        public ExemplarService(IExemplarRepository exemplarRepository)
        {
            this.exemplarRepository = exemplarRepository;
        }

        public Exemplar Alterar(Exemplar exemplar)
        {
            return this.exemplarRepository.Alterar(exemplar);
        }

        public Exemplar BuscarPorId(int id)
        {
            return this.exemplarRepository.BuscarPorId(id);
        }

        public IEnumerable<Exemplar> BuscarPorIdLivro(int idLivro)
        {
            return this.exemplarRepository.BuscarPorIdLivro(idLivro);
        }

        public Exemplar Excluir(int id)
        {
            var exemplar = this.exemplarRepository.BuscarPorId(id);
            exemplar.Ativo = false;
            return this.exemplarRepository.Excluir(exemplar);
        }

        public Exemplar Incluir(Exemplar exemplar)
        {
            var exexmplarJaCadastradoOutroTitulo = this.exemplarRepository.IsExisteExemplarCadastroParaOutroTitulo(exemplar.Numero);
            if (exexmplarJaCadastradoOutroTitulo)
            {
                throw new Exception("Ja existe um exemplar cadastrado com esse número");
            }

            return this.exemplarRepository.Incluir(exemplar);
        }
    }
}
