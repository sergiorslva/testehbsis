﻿using System.Collections.Generic;

namespace HBSIS.Domain.Model
{
    public class Livro
    {
        public int Id { get; set; }
        public string  Titulo { get; set; }
        public int Ano { get; set; }
        public string Escritor { get; set; }
        public bool Ativo { get; set; }

        public List<Exemplar> Exemplares { get; set; }
    }
}
