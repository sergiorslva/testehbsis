﻿namespace HBSIS.Domain.Model
{
    public class Exemplar
    {
        public int Id { get; set; }
        public int LivroId { get; set; }
        public string Numero { get; set; }
        public bool Ativo { get; set; }

        public Livro Livro { get; set; }
    }
}
