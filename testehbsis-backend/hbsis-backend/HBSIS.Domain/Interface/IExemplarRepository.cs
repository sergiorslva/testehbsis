﻿using HBSIS.Domain.Model;
using System.Collections.Generic;

namespace HBSIS.Domain.Interface
{
    public interface IExemplarRepository : IRepository<Exemplar>
    {
        bool IsExisteExemplarCadastroParaOutroTitulo(string numero);
        IEnumerable<Exemplar> BuscarPorIdLivro(int idLivro);
    }
}
