﻿using HBSIS.Domain.Model;
using System.Collections.Generic;

namespace HBSIS.Domain.Interface
{
    public interface ILivroService
    {
        Livro Incluir(Livro livro);
        Livro Alterar(Livro livro);
        Livro Excluir(int id);
        Livro BuscarPorId(int id);
        IEnumerable<Livro> BuscarTodos();
    }
}
