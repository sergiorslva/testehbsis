﻿using HBSIS.Domain.Model;

namespace HBSIS.Domain.Interface
{
    public interface ILivroRepository: IRepository<Livro>
    {
        bool IsExisteTituloCadastroComMesmoTitulo(string titulo);
    }
}
