﻿using System;
using System.Collections.Generic;

namespace HBSIS.Domain.Interface
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        TEntity Incluir(TEntity obj);
        TEntity Alterar(TEntity obj);
        TEntity Excluir(TEntity obj);
        TEntity BuscarPorId(int id);
        IEnumerable<TEntity> BuscarTodos();
    }
}
