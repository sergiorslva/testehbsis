﻿using HBSIS.Domain.Model;
using System.Collections.Generic;

namespace HBSIS.Domain.Interface
{
    public interface IExemplarService
    {
        Exemplar Incluir(Exemplar exemplar);
        Exemplar Alterar(Exemplar exemplar);
        Exemplar Excluir(int id);
        Exemplar BuscarPorId(int id);
        IEnumerable<Exemplar> BuscarPorIdLivro(int idLivro);
    }
}
