﻿using HBSIS.Infra.CrossCutting.IoC;
using Microsoft.Extensions.DependencyInjection;

namespace HBSIS.Presentation.API.Configurations
{
    public static class DependencyInjectionConfiguration
    {
        public static void AddDIConfiguration(this IServiceCollection services)
        {
            NativeInjectorBootStrapper.RegisterServices(services);
        }
    }
}
