﻿using HBSIS.Application.Interface;
using HBSIS.Application.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace HBSIS.Presentation.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LivrosController : Controller
    {
        private readonly ILivroApp livroApp;

        public LivrosController(ILivroApp livroApp)
        {
            this.livroApp = livroApp;
        }

        [HttpGet]
        public IEnumerable<LivroViewModel> Get()
        {
            return this.livroApp.BuscarTodos();
        }

        [HttpGet("{id}")]
        public LivroViewModel Get(int id)
        {
            return this.livroApp.BuscarPorId(id);
        }

        [HttpPost]
        public LivroViewModel Post(LivroViewModel livro)
        {
            var novoLivro = this.livroApp.Incluir(livro);
            return novoLivro;
        }

        [HttpPut]
        public LivroViewModel Put(LivroViewModel livro)
        {            
            return this.livroApp.Alterar(livro);
        }

        [HttpDelete("{id}")]
        public LivroViewModel Delete(int id)
        {
            return this.livroApp.Excluir(id);
        }
    }
}