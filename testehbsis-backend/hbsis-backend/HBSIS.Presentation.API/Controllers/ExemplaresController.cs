﻿using HBSIS.Application.Interface;
using HBSIS.Application.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace HBSIS.Presentation.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExemplaresController : Controller
    {
        private readonly IExemplarApp exemplarApp;
        private object livroApp;

        public ExemplaresController(IExemplarApp exemplarApp)
        {
            this.exemplarApp = exemplarApp;
        }

        [HttpGet("livro/{idlivro}")]
        public IEnumerable<ExemplarViewModel> GetPorIdLivro(int idlivro)
        {
            return this.exemplarApp.BuscarTodosPorLivro(idlivro);
        }

        [HttpGet("{id}")]
        public ExemplarViewModel Get(int id)
        {
            return this.exemplarApp.BuscarPorId(id);
        }

        [HttpPost]
        public ExemplarViewModel Post(ExemplarViewModel livro)
        {
            return this.exemplarApp.Incluir(livro);
        }

        [HttpPut]
        public ExemplarViewModel Put(ExemplarViewModel livro)
        {
            return this.exemplarApp.Alterar(livro);
        }

        [HttpDelete("{id}")]
        public ExemplarViewModel Delete(int id)
        {
            return this.exemplarApp.Excluir(id);
        }
    }
}