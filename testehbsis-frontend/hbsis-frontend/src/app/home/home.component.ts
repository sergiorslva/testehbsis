import { Component, OnInit } from '@angular/core';
import { LivroService } from '../services/livro.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public livros: any;

  constructor(private livroService: LivroService) { }

  ngOnInit() {
    this.listarLivros();
  }

  listarLivros(){
    this.livroService.listarTodos().subscribe(response => {
      this.livros = response;
      console.log(this.livros);
    })
  }

}
