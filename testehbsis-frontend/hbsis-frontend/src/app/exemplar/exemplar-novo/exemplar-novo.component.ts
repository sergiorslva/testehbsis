import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ExemplarService } from '../../services/exemplar.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-exemplar-novo',
  templateUrl: './exemplar-novo.component.html',
  styleUrls: ['./exemplar-novo.component.css']
})
export class ExemplarNovoComponent implements OnInit {

  modoEdicao: boolean = false;
  livroId: any;
  exemplarId: any;

  exemplarForm = new FormGroup({
    id: new FormControl(''),
    livroId: new FormControl(''),
    numero: new FormControl(''),    
    ativo: new FormControl('')
  });


  constructor(
    private exemplarService: ExemplarService,    
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe( params => {
      if(params.id){
        this.carregarFormulario(params.id)
        this.modoEdicao = true;
        this.exemplarId = params.id;
      }     
      
      if(params.idlivro){
        this.livroId = params.idlivro;
      }
    })
  }

  carregarFormulario(id: number){
    this.exemplarService.listarPorId(id).subscribe(
      response => {        
        this.exemplarForm.setValue({
          id: response.id,
          numero: response.numero,
          livroId: response.livroId,          
          ativo: response.ativo
        })
      }, 
      error => {
        alert("Não foi possível carregar os valores do exemplar selecionado")       
      }
    )
  }

  incluirLivro(){    
    this.exemplarForm.patchValue({id: 0});
    this.exemplarForm.patchValue({livroId: this.livroId});
    this.exemplarForm.patchValue({ativo: true});

    this.exemplarService.incluir(this.exemplarForm.value).subscribe(response => {
      alert("Registro incluido com sucesso!")
      this.router.navigate(['/exemplar/', this.livroId])
    }, error => {
      alert("Não foi possível incluir o registro")
    })
  }

  editarLivro(){
    this.exemplarService.editar(this.exemplarForm.value).subscribe(response => {
      alert("Registro atualizado com sucesso!");
      this.router.navigate(['/exemplar/', this.exemplarId])
    }, error => {
      alert("Não foi possível atualizar o registro")
    })
  }

  onSubmit(){
    if (this.modoEdicao){
      this.editarLivro();
    }else{
      this.incluirLivro();
    }    
  }

}
