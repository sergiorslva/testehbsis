import { Component, OnInit } from '@angular/core';
import { ExemplarService } from '../../services/exemplar.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LivroService } from '../../services/livro.service';

@Component({
  selector: 'app-exemplar-lista',
  templateUrl: './exemplar-lista.component.html',
  styleUrls: ['./exemplar-lista.component.css']
})
export class ExemplarListaComponent implements OnInit {

  public exemplares: any = [];
  public livro: any

  constructor(
    private exemplarService: ExemplarService,
    private livroService: LivroService,    
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe( params => {
      if(params.idlivro){
        this.carregarDadosLivro(params.idlivro)        
      }      
    })
  }

  carregarDadosLivro(idlivro: any): any {
    this.livroService.listarPorId(idlivro).subscribe(response => {
      this.livro = response;
      this.listarExemplares(this.livro.id);
    }, error => {
      alert("Não foi possível carregar os exemplares do livro selecionado")
    })
  }

  listarExemplares(idLivro){    
    this.exemplarService.listarTodosPorIdLivro(idLivro).subscribe(response => {
      this.exemplares = response;
    }, error => {
      alert("Não foi possível carregar os exemplares do livro selecionado")
    })
  }

  excluir(exemplar:any){
    if(confirm("Confirmar a exclusão do exemplar " + exemplar.numero)) {
      this.exemplarService.excluir(exemplar.id).subscribe(response => {
        alert("Exemplar excluido com sucesso!");
        this.listarExemplares(this.livro.id);
      }, error => {
        alert("Não foi possível excluir o exemplar!")
      })
    }
  }

}
