import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { rootRouterConfig } from './app.routes';
import {NgxPaginationModule} from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { TemplateComponent } from './template/template.component';
import { ExemplarService } from './services/exemplar.service';
import { LivroService } from './services/livro.service';
import { HttpClientModule } from '@angular/common/http';
import { LivroListaComponent } from './livro/livro-lista/livro-lista.component';
import { LivroNovoComponent } from './livro/livro-novo/livro-novo.component';
import { ExemplarListaComponent } from './exemplar/exemplar-lista/exemplar-lista.component';
import { ExemplarNovoComponent } from './exemplar/exemplar-novo/exemplar-novo.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TemplateComponent,
    LivroListaComponent,
    LivroNovoComponent,
    ExemplarListaComponent,
    ExemplarNovoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,  
    NgxPaginationModule,  
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(rootRouterConfig, { useHash: false }),
  ],
  providers: [LivroService, ExemplarService],
  bootstrap: [AppComponent]
})
export class AppModule { }
