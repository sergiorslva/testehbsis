import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class LivroService {

  constructor(public httpClient: HttpClient) { }

  public incluir(form) : Observable<any>{        
    let url = `${environment.urlAPI}livros`;    
    return this.httpClient.post(url, form);             
  }

  public editar(form) : Observable<any>{    
    let url = `${environment.urlAPI}livros`;    
    return this.httpClient.put(url, form);             
  }

  public excluir(id) : Observable<any>{    
    let url = `${environment.urlAPI}livros/${id}`;    
    return this.httpClient.delete(url);             
  }

  public listarTodos() : Observable<any>{    
    let url = `${environment.urlAPI}livros`;   
    console.log(url); 
    return this.httpClient.get(url);             
  }

  public listarPorId(id) : Observable<any>{    
    let url = `${environment.urlAPI}livros/${id}`;    
    return this.httpClient.get(url);             
  }

}
