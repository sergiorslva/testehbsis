import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';

@Injectable()
export class ExemplarService {

  constructor(public httpClient: HttpClient) { }

  public incluir(form) : Observable<any>{    
    let url = `${environment.urlAPI}exemplares`;    
    return this.httpClient.post(url, form);             
  }

  public editar(form) : Observable<any>{    
    let url = `${environment.urlAPI}exemplares`;    
    return this.httpClient.put(url, form);             
  }

  public excluir(id) : Observable<any>{    
    let url = `${environment.urlAPI}exemplares/${id}`;    
    return this.httpClient.delete(url);             
  }

  public listarTodosPorIdLivro(idLivro) : Observable<any>{    
    let url = `${environment.urlAPI}exemplares/livro/${idLivro}`;   
    console.log(url); 
    return this.httpClient.get(url);             
  }

  public listarPorId(id) : Observable<any>{    
    let url = `${environment.urlAPI}exemplares/${id}`;    
    return this.httpClient.get(url);             
  }

}
