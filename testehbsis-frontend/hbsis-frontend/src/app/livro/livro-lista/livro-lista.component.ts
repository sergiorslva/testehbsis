import { Component, OnInit } from '@angular/core';
import { LivroService } from '../../services/livro.service';

@Component({
  selector: 'app-livro-lista',
  templateUrl: './livro-lista.component.html',
  styleUrls: ['./livro-lista.component.css']
})
export class LivroListaComponent implements OnInit {

  public livros: any = [];
  
  constructor(private livroService: LivroService) {   
  }

  ngOnInit() {
    this.listarLivros();
  }

  listarLivros(){    
    this.livroService.listarTodos().subscribe(response => {
      this.livros = response;
    })
  }

  excluir(livro:any){
    if(confirm("Confirmar a exclusão do livro " + livro.titulo)) {
      this.livroService.excluir(livro.id).subscribe(response => {
        alert("Livro excluido com sucesso!");
        this.listarLivros();
      }, error => {
        alert("Não foi possível excluir o livro!")
      })
    }
  }

}
