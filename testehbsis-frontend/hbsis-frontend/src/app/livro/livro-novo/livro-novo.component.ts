import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { LivroService } from '../../services/livro.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-livro-novo',
  templateUrl: './livro-novo.component.html',
  styleUrls: ['./livro-novo.component.css']
})
export class LivroNovoComponent implements OnInit {
  
  public anos: number[] = [];
  modoEdicao: boolean = false;

  livroForm = new FormGroup({
    id: new FormControl(''),
    titulo: new FormControl(''),
    ano: new FormControl(''),
    escritor: new FormControl(''),
    ativo: new FormControl('')
  });


  constructor(
    private livroService: LivroService,    
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe( params => {
      if(params.id){
        this.carregarFormulario(params.id)
        this.modoEdicao = true;
      }      
    })
    this.carregarAnos();
  }
  
  carregarAnos(){
    let anoInicial = 1980;
    let anoAtual = new Date().getFullYear();
    let diferencaEntreAnos = anoAtual - anoInicial;

    for(let i = 0; i <= diferencaEntreAnos; i++){
      this.anos.push(anoInicial+i)
    }
  }

  carregarFormulario(id: number){
    this.livroService.listarPorId(id).subscribe(
      response => {        
        this.livroForm.setValue({
          id: response.id,
          titulo: response.titulo,
          ano: response.ano,
          escritor: response.escritor,
          ativo: response.ativo
        })
      }, 
      error => {
        alert("Não foi possível carregar os valores do livro selecionado")       
      }
    )
  }

  incluirLivro(){    
    this.livroForm.patchValue({id: 0});
    this.livroForm.patchValue({ativo: true});

    this.livroService.incluir(this.livroForm.value).subscribe(response => {
      alert("Registro incluido com sucesso!");
      this.router.navigate(['/livro']);
    }, error => {
      alert("Não foi possível incluir o registro")
    })
  }

  editarLivro(){
    this.livroService.editar(this.livroForm.value).subscribe(response => {
      alert("Registro atualizado com sucesso!");
      this.router.navigate(['/livro']);
    }, error => {
      alert("Não foi possível atualizar o registro")
    })
  }

  onSubmit(){
    if (this.modoEdicao){
      this.editarLivro();
    }else{
      this.incluirLivro();
    }    
  }

}
