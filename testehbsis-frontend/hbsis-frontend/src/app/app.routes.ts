import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { TemplateComponent } from './template/template.component';
import { LivroListaComponent } from './livro/livro-lista/livro-lista.component';
import { LivroNovoComponent } from './livro/livro-novo/livro-novo.component';
import { ExemplarListaComponent } from './exemplar/exemplar-lista/exemplar-lista.component';
import { ExemplarNovoComponent } from './exemplar/exemplar-novo/exemplar-novo.component';


//Components
export const rootRouterConfig: Routes = [
        
    { path: '', redirectTo: 'livro', pathMatch: 'full' },
    {
        path: '',
        component: TemplateComponent,
        children: [
            { path: 'livro', component: LivroListaComponent },
            { path: 'livro/novo', component: LivroNovoComponent},                             
            { path: 'livro/editar/:id', component: LivroNovoComponent},

            { path: 'exemplar/:idlivro', component: ExemplarListaComponent },
            { path: 'exemplar/novo/:idlivro', component: ExemplarNovoComponent},                             
            { path: 'exemplar/editar/:id', component: ExemplarNovoComponent}            
        ],         
    }, 
]